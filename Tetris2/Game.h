#pragma once

#include "Scenes/Scenes.h"
#include "Types.h"
#include <map>
#include <string.h>

//#include <win

#define GAME Game::GetInstance()

class Game
{
public:

	static Game* GetInstance();
	static void DeleteInstance();

	void Event(GameEvent event);
	
	void Init(HWND hWnd);
	void Draw();

	Scene *GetActiveScene();

private:
	Game();
	~Game();

private:

	Scene * activeScene;

	static Game *instance;

	HWND _hWnd;

	std::map<char*, Scene*>scenes;

	Scene *menuScene;
	Scene *gameScene;
	Scene *endScene;

};

