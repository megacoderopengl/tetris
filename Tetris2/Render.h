#pragma once

#include <Windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include "Types.h"

#define RENDER Render::GetInstance()

class Render
{
public:

	static Render* GetInstance();
	static void DeleteInstance();

	GLvoid PrintText(float x, float y, const char *fmt, ...);
	
	bool Init(int windowW, int windowH, HDC hdc);

	Size GetSize();
	void Draw();

	void DrawRectangle(const Point &a, const Point &b, const GLfloat z, const Color &color);
	

private:
	Render();
	~Render();

	GLvoid BuildFont(GLvoid);
	GLvoid KillFont(GLvoid);

	static Render *instance;

	HDC hDC;  // Приватный контекст устройства GDI

	Size size;

	GLuint  base;

};

