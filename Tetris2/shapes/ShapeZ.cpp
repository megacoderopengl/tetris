#include "ShapeZ.h"
#include <iostream>

ShapeZ::ShapeZ()
{
	SetSize(3,2);
	sizeMem = GetSize().w * GetSize().h;
	rect = new uchar[sizeMem];
	undoRect = new uchar[sizeMem];
}


ShapeZ::~ShapeZ()
{
}

void ShapeZ::Init()
{
	//
	//    = =
	//      =  =
	//

	memset(rect, 0, sizeMem);
	memset(rect, 1, 2);
	memset(rect + GetSize().w +1, 1, 2);

	RandRotate(3);
}