#include "Shape.h"
#include <cstring>
#include <iostream>
#include "..\Render.h"

Shape::Shape()
{
	size.w = SHAPE_W;
	size.h = SHAPE_H;

	memset(&rect, 0, SHAPE_SIZE);
}


Shape::~Shape()
{

}


void Shape::Init()
{

	rect = new uchar[int(size.w * size.h)];
	undoRect = new uchar[int(size.w * size.h)];
}

void Shape::Draw()
{
	float gX = grCoor.x;
	float gY = grCoor.y;
	float ofX = partSize.x;
	float ofY = partSize.y;
	
	for (int i = 0; i < size.h; ++i)
	{
		for (int j = 0; j < size.w; ++j)
		{
			if (1 == rect[size.w * i + j])
			{
				RENDER->DrawRectangle(Point(gX + ofX * 0.2, gY + ofY * 0.2), Point(gX + ofX *0.8, gY + ofY * 0.8), 1, Color(0.5, 0.0, 0.0));
				RENDER->DrawRectangle(Point(gX, gY), Point(gX + ofX, gY + ofY), 1, Color(0.0, 0.3, 0.0));
			}
			gX += ofX;
		}
		gX = grCoor.x;
		gY += ofY;
	}
}

void Shape::Rotate()
{
	uchar buf;
	
	uchar *temp = new uchar[sizeMem];

	memcpy(undoRect, rect, sizeMem);
	undoSize.h = size.h;
	undoSize.w = size.w;

	uchar tW = size.h;
	uchar tH = size.w;
	
	uchar tX = 0;
	uchar tY = 0;
		
	tX = tW -1;
	
	for (uchar i = 0; i < size.h; ++i)
	{
		for (uchar j = 0; j < size.w; ++j)
		{
			int xx = tW * tY + tX;

			int yy = size.w * i + j;

			temp[xx] = rect[yy];
			
			++tY;
		}
		tY = 0;
		-- tX;
	}

	memcpy(rect, temp, sizeMem);

	delete[]temp;

	size.w = tW;
	size.h = tH;
}


void Shape::RandRotate(uchar countRotation)
//countRotation - ���������� ��������� ��������� ������
{
	for (uchar i = 0; i < countRotation; ++i)
	{
		uchar needRotate = rand() % 2;

		if (needRotate) //����� ������ ���������� � ������������ ���������
		{
			Rotate();
		}
	}

	
}


uchar Shape::GetPoint(uchar x, uchar y)
{
	return rect[size.w * y + x];
}

Point Shape::GetGlobalPosition()
{
	return globalCoor;
}

void Shape::SetGlobalPosition(uchar x, uchar y)
{
	globalCoor.x = x;
	globalCoor.y = y;
}



uchar *Shape::GetRect() 
{
	return rect;
}

void Shape::SetSize(uchar w, uchar h)
{
	size.w = w, size.h = h;
}

Size Shape::GetSize()
{
	return size;
}

void Shape::UndoRotate()
{
	memcpy(rect, undoRect, sizeMem);
	size.h = undoSize.h;
	size.w = undoSize.w;
}


Pointf Shape::GrPosition()
{
	return grCoor;
}

void Shape::SetGrPosition(Pointf coor)
{
	grCoor = coor;
}

void Shape::SetpartSize(Pointf size)
{
	partSize = size;
}

Pointf Shape::GetpartSize()
{
	return partSize;
}