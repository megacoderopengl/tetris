#include "ShapeJ.h"
#include <cstring>


ShapeJ::ShapeJ()
{
	SetSize(3, 2);
	sizeMem = GetSize().w * GetSize().h;
	rect = new uchar[sizeMem];
	undoRect = new uchar[sizeMem];
}


ShapeJ::~ShapeJ()
{
}

void ShapeJ::Init()
{
	//
	// = = =
	//     N
	//

	memset(rect, 1, SHAPE_SIZE);
	memset(rect + GetSize().w, 0, 2);
	

	RandRotate(3);
}
