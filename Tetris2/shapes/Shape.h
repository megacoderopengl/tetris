#pragma once

#include "../SceneItem.h"
#include "../Types.h"

#include <Windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>


const uchar SHAPE_W = 4; //������ ���� ������
const uchar SHAPE_H = 4; //������ ���� ������
const uchar SHAPE_SIZE = SHAPE_H * SHAPE_W;

class Shape
{
public:
	Shape();
	~Shape();
		

	virtual void Init();
	virtual void Draw();
	virtual void Rotate();
	
	uchar GetPoint(uchar x, uchar y);

	Point GetGlobalPosition();
	void SetGlobalPosition(uchar x, uchar y);

	void SetSize(uchar w, uchar h);
	Size GetSize();

	Pointf GrPosition();
	void SetGrPosition(Pointf coor);

	void SetpartSize(Pointf size);
	Pointf GetpartSize();
		
	uchar *GetRect();

	void UndoRotate();



protected:
	
	void RandRotate(uchar countRotations);

	uchar *rect;
	Point globalCoor; //���������� ���� ������ ������������ ������� �������� ����  (������� ����� ����)
	Pointf grCoor; //����������� ���������� ������� ����� �����
	uchar sizeMem; // ���������� ������ ����������� ��� ���� ������
	uchar *undoRect;
	Size undoSize;

	Pointf partSize;
private:
	Size size;

	
	
	
};