#include "ShapeS.h"
#include <iostream>

ShapeS::ShapeS()
{
	SetSize(3, 2);
	sizeMem = GetSize().w * GetSize().h;
	rect = new uchar[sizeMem];
	undoRect = new uchar[sizeMem];
}


ShapeS::~ShapeS()
{
}

void ShapeS::Init()
{
	//
	//   = =
	// = =
	//

	memset(rect, 0, sizeMem);
	memset(rect + 1, 1, 2);
	memset(rect + GetSize().w, 1, 2);

	RandRotate(1);

}