#pragma once
#include "Scene.h"
class EndScene : public Scene
{
public:
	EndScene();
	~EndScene();

	void Event(GameEvent event);
	void Draw();
};

