#pragma once

#include "../SceneItem.h"
#include <list>


class Scene
{
public:
	Scene();
	~Scene();

	virtual void Init() {}

	virtual void Draw() {}

	virtual void Event(GameEvent event);
	
	void AddItems(SceneItem *itm);

	std::list<SceneItem *> *GetChild();

protected:

	std::list<SceneItem *> sceneItems;
};