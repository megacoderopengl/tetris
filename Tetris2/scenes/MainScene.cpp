#include "MainScene.h"
#include "..\Render.h"
#include  "..\Game.h"


MainScene::MainScene()
{
}


MainScene::~MainScene()
{
}


void MainScene::Event(GameEvent event)
{
	switch (event)
	{
		case evDown:
		{
			int m = static_cast<int>(stateMenu);
			if (m + 1 == MenuSize)
				m = 0;
			else
				++m;
			stateMenu = static_cast<MenuItems>(m);
			break;
		}

		case evUp:
		{
			int m = static_cast<int>(stateMenu);
			if (m - 1 == -1)
				m = MenuSize - 1;
			else
				--m;
			stateMenu = static_cast<MenuItems>(m);
			break;
		}
		case evRight:
		{
			if (eStart == stateMenu)
				GAME->Event(evGameScene);

			break;
		}
	}
}

void MainScene::Draw()
{
	//������ ������
	Point a = Point(0, 0 ); 
	Point b = Point(RENDER->GetSize().w * 0.2, RENDER->GetSize().h * 0.1);

	//������ ������ * �� ���-�� ������ �������������� �� Y �� ������
	float offSetY = b.y - a.y + RENDER->GetSize().h * 0.05 ;

	float offSetX = (RENDER->GetSize().w - b.x) * 0.5;

	float offSetTxtX = (b.x - a.x) * 0.35;
	float offSetTxtY = (b.y - a.y) * 0.6;

	a.x += offSetX; b.x += offSetX;
	
	Color btnColor = Color(0.5, 0.5, 0.5);
	Color btnColorActive = Color(0.0, 0.2, 0.0);
	Color color;
	a.y += offSetY;
	b.y += offSetY;


	if (eStart == stateMenu) color = btnColorActive; 
		else color = btnColor;

	DrawBtn(a, b, color, "START");
	
	a.y += offSetY;
	b.y += offSetY;

	if (eAbout == stateMenu) color = btnColorActive; 
		else color = btnColor;

	DrawBtn(a, b, color, "ABOUT");
		

	a.y += offSetY;
	b.y += offSetY;
	if (eExit == stateMenu) color = btnColorActive; 
		else color = btnColor;

	DrawBtn(a, b, color, "EXIT");
	
}

void MainScene::Init()
{
	stateMenu = eStart;
}

void MainScene::DrawBtn(Point a, Point b, Color color, char* txt)
{
	float offSetTxtX = (b.x - a.x) * 0.35;
	float offSetTxtY = (b.y - a.y) * 0.6;

	glColor3f(color.r + 0.2, color.g + 0.2, color.b + 0.2);
	RENDER->PrintText(a.x + offSetTxtX, a.y + offSetTxtY, txt);
	RENDER->DrawRectangle(a, b, 0, color);
}