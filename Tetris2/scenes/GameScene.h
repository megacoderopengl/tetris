#pragma once

#include <list>
#include "Scene.h"
#include "../SceneItem.h"
#include "Scenes.h"


class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();

	void Init();
	void Draw();
	

	inline void AddItems(SceneItem *itm);

	std::list<SceneItem *> *GetChild();

private:

	


};