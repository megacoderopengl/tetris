#pragma once

#include "Types.h"


class SceneItem
{
public:
	SceneItem();
	~SceneItem();

	virtual void Init(){};
	virtual void Draw(){};
	virtual void Tick(){};

	virtual void Event(GameEvent event){}

	Size GetSize();
	void SetSize(int w, int h);

	inline void DeleteLater();
	inline bool IsDelete() const;



private:
	bool isdelete;
	Size size;
};

