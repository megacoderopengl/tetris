#pragma once

#include <Windows.h>
#include "SceneItem.h"
#include "Types.h"

#include "shapes\ShapeAll.h"
#include "shapes\Shape.h"
//#include<gl\GL.h>


const uchar GAME_RECT_W = 10;
const uchar GAME_RECT_H = 18;


class RectGame : public SceneItem
{
public:
	RectGame();
	~RectGame();

	void Init();
	void Draw();
	

	bool CreateShape();
	bool IsDownCollision(int ox, int oy);
	void CrashFullLine();
	void AttachShape();
	
	bool MoveDownShape();
	bool MoveLeftShape();
	bool MoveRightShape();
	
	bool MoveShape(int x, int y);

	void UpdateActiveShapeGrCoor();

	void Event(GameEvent event);

	void SetGeometry(Geometry &g);
	Geometry GetGeometry();

	UINT32 GetScope();

	Shape *GetActiveShape();
	uchar *GetRect();
	

private:

	enum CollectionShape
	{
		eShapeI = 0,
		eShapeJ,
		eShapeL,
		eShapeO,
		eShapeS,
		eShapeT,
		eShapeZ,

		eShapeSize
	};
	
	Shape *activeShape;

	uchar rect[GAME_RECT_H ][ GAME_RECT_W];

	unsigned int scope;

	Geometry geometry;

	Point pointSpawn;

	Pointf partSize;

	int speed;

	int nTimerID;
	int level;
	
};

